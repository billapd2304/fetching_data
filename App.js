import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table } from 'flowbite-react';
import './App.css';

const App = () => {
  const [data, setData] = useState(null);

  const getCondition = (score) => {
    if (score >= 80) {
      return 'A';
    } else if (score >= 70) {
      return 'B';
    } else if (score >= 60) {
      return 'C';
    } else if (score >= 50) {
      return 'D';
    } else {
      return 'E';
    }
  };

  const handleIndexScore = (score) => {
    console.log(`Condition: ${getCondition(score)}`);
  };

  useEffect(() => {
    axios.get("https://backendexample.sanbercloud.com/api/student-scores")
      .then((res) => {
        setData([...res.data]);
        res.data.forEach((student) => {
          handleIndexScore(student.score);
        });
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  return (
    <div className="overflow-x-auto">
      <Table style={{ width: '100%' }}>
        <Table.Head>
          <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>nama</Table.HeadCell>
          <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>score</Table.HeadCell>
          <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>Condition</Table.HeadCell>
        </Table.Head>
        <Table.Body className="divide-y">
          {data !== null && data.map((res) => (
            <Table.Row key={res.id}>
              <Table.Cell style={{ border: '1px solid #dddddd' }}>{res.name}</Table.Cell>
              <Table.Cell style={{ border: '1px solid #dddddd' }}>{res.score}</Table.Cell>
              <Table.Cell style={{ border: '1px solid #dddddd' }}>{getCondition(res.score)}</Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    </div>
  );
};

export default App;
