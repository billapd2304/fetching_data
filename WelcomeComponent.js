import React from 'react'
import Welcome from './WelcomeComponent';

function App()){
    return (
        <div>
            <Welcome name="Salsa"/>
            <Welcome name="Billah"/>
            <Welcome name="Pacar Sefriwal dan Sunghoon"/>
        </div>
    )
}
export default App;